package com.example.demo.mappers;

import com.example.demo.model.Account;
import com.example.demo.model.Person;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Set;


@Mapper
public interface PersonMapper {

    @Insert("INSERT INTO person(first_name, last_name) VALUES (#{firstName}, #{lastName})")
    int addPerson(Person person);


    @Select("SELECT * FROM person")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "accounts", column = "ID",
                    javaType = Set.class,
                    many = @Many(select = "findAccountsByPersonId"))})
    List<Person> findAllPersons();


    @Select("SELECT * FROM person WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "accounts", column = "ID",
                    javaType = Set.class,
                    many = @Many(select = "findAccountsByPersonId"))})
    Person findById(@Param("id") int id);


    @Select("SELECT * FROM person WHERE first_name = #{firstName}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "accounts", column = "ID",
                    javaType = Set.class,
                    many = @Many(select = "findAccountsByPersonId"))})
    List<Person> findByFirstName(@Param("firstName") String firstName);


    @Select("SELECT * FROM person WHERE first_name = #{firstName} AND last_name = #{lastName}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "accounts", column = "ID",
                    javaType = Set.class,
                    many = @Many(select = "findAccountsByPersonId"))})
    Person findByFullName(@Param("firstName") String firstName,
                          @Param("lastName") String lastName);


    @Select("SELECT * FROM account WHERE id_person = #{personId}")
    Set<Account> findAccountsByPersonId(@Param("personId") int personId);


    @Select("CALL mybatis.findAllNames()")
    List<String> findAllFirstNames();

}
