package com.example.demo.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Account {

    private Integer id;
    private Integer idPerson;
    private Integer cardNumber;
    private Integer value;

}
