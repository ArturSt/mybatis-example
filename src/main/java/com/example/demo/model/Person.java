package com.example.demo.model;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class Person {

    private Integer id;
    private String firstName;
    private String lastName;
    private Set<Account> accounts;

}
