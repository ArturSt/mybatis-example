package com.example.demo.dao;

import com.example.demo.model.Person;
import lombok.AllArgsConstructor;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;

import java.util.List;

@AllArgsConstructor
@Component
public class PersonDao {

    private final SqlSession sqlSession;

    public Person getPersonById(Integer id) {
        return sqlSession.selectOne("findById", id);
    }

    public List<Person> getPersonByFirstName(String firstName) {
        return sqlSession.selectList("findByFirstName", firstName);
    }

    public List<Person> getAllPersons() {
        return sqlSession.selectList("findAllPersons");
    }

    public List<Person> getAllPersonsByRowBounds(RowBounds rowBounds) {
        return sqlSession.selectList("findAllPersons", null, rowBounds);
    }

    public List<String> getAllFirstNames() {
        return sqlSession.selectList("findAllFirstNames");
    }

    public int addPerson(Person build) {
        return sqlSession.insert("addPerson");
    }

}
