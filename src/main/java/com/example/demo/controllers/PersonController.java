package com.example.demo.controllers;

import com.example.demo.model.Person;
import com.example.demo.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/mybatis/person")
public class PersonController {

    @Autowired
    private PersonService personService;


    @GetMapping()
    public ResponseEntity<List<Person>> getAllPersons() {
        return ResponseEntity.ok(personService.getAllPersons());
    }

    @GetMapping(value = "/offset/{offset}/limit/{limit}")
    public ResponseEntity<List<Person>> getAllPersons(@PathVariable("offset") Integer offset,
                                                      @PathVariable("limit") Integer limit) {
        return ResponseEntity.ok(personService.getAllPersons(offset, limit));
    }

    @GetMapping(value = "/ids/{id}")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(personService.getPersonById(id));
    }

    @GetMapping(value = "/first-names/callable")
    public ResponseEntity<List<String>> getAllFirstNames() {
        return ResponseEntity.ok(personService.getAllFirstNames());
    }

    @GetMapping(value = "/first-names/{firstName}")
    public ResponseEntity<List<Person>> getPersonByFirstName(@PathVariable("firstName") String firstName) {
        return ResponseEntity.ok(personService.getPersonsByFirstName(firstName));
    }

    @PostMapping(value = "/add/first-name/{firstName}/last-name/{lastName}")
    public ResponseEntity<Integer> addPerson(@PathVariable("firstName") String firstName,
                                             @PathVariable("lastName") String lastName) {
        return ResponseEntity.ok(personService.addPerson(firstName, lastName));
    }

}
