package com.example.demo.services;

import com.example.demo.dao.PersonDao;
import com.example.demo.model.Person;
import lombok.AllArgsConstructor;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Service;

import java.util.List;


@AllArgsConstructor
@Service
public class PersonService {

    private final PersonDao personDao;


    public List<Person> getAllPersons() {
        return personDao.getAllPersons();
    }

    public List<Person> getAllPersons(int offset, int limit) {
        return personDao.getAllPersonsByRowBounds(new RowBounds(offset, limit));
    }

    public Person getPersonById(Integer id) {
        return personDao.getPersonById(id);
    }

    public List<Person> getPersonsByFirstName(String firstName) {
        return personDao.getPersonByFirstName(firstName);
    }

    public List<String> getAllFirstNames() {
        return personDao.getAllFirstNames();
    }

    public int addPerson(String firstName, String lastName) {
        return personDao.addPerson(Person.builder()
                .firstName(firstName)
                .lastName(lastName)
                .build());
    }

}
