drop table IF EXISTS ACCOUNT;

create TABLE ACCOUNT(
    ID              int     NOT NULL    AUTO_INCREMENT,
    ID_PERSON       int     NOT NULL,
    CARD_NUMBER     int     NOT NULL,
    VALUE           int     NOT NULL,
    primary key(ID));

drop table IF EXISTS PERSON;

create TABLE PERSON(
	ID              int             NOT NULL    AUTO_INCREMENT,
    FIRST_NAME      VARCHAR(255)    NOT NULL,
    LAST_NAME       VARCHAR(255)    NOT NULL,
    mock            int,
    primary key(ID));