insert into person (first_name, last_name)
values              ('Adam',    'Smith'),
                    ('Ivan',    'Danilov'),
                    ('Gustav',  'Aarch'),
                    ('Arnold',  'Danov'),
                    ('Aleh',    'Valov'),
                    ('Anna',    'Nilov'),
                    ('Grisha',  'Kanilov'),
                    ('Sergey',  'Ranov'),
                    ('Niko',    'Pilov'),
                    ('Kirill',  'Lanilov'),
                    ('Stas',    'Milov'),
                    ('Illia',   'Kilov'),
                    ('Yan',     'Warilov'),
                    ('Vitaly',  'Janilov'),
                    ('Jared',   'Fanilov'),
                    ('Vasya',   'Silov'),
                    ('Vadim',   'Balov'),
                    ('Ioann',   'Cilov'),
                    ('Vlad',    'Zilov'),
                    ('Michael', 'Ganilov'),
                    ('Emel',    'Usanov'),
                    ('Petr',    'Ivanov');

insert into account (id_person, card_number, value)
values              (1,     23456789,   98765456),
                    (2,     234567,     3245654),
                    (2,     3462345,    323554),
                    (3,     2235567,    322354),
                    (3,     2523525,    323554),
                    (4,     2124567,    3225),
                    (5,     65435567,   3254),
                    (6,     92367,      32254),
                    (6,     3267,       254),
                    (7,     8764567,    32654),
                    (8,     345567,     2364),
                    (9,     57634567,   23564),
                    (9,     6564457,    2364),
                    (10,    564534,     323464),
                    (11,    54543545,   4598754),
                    (12,    5647,       425654),
                    (13,    13243,      24635654),
                    (14,    79086567,   23464),
                    (15,    2869767,    5637654),
                    (16,    978667,     234654),
                    (17,    75647,      3654),
                    (17,    5465367,    5654),
                    (17,    5463567,    3245),
                    (18,    463567,     5654),
                    (19,    24653,      5654),
                    (20,    4653567,    57655654),
                    (20,    75667,      235654),
                    (20,    675467,     987654),
                    (21,    6757,       125654),
                    (21,    7667,       24553464),
                    (21,    9867,       346654);
